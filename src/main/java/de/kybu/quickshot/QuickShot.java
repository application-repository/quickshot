package de.kybu.quickshot;

import de.kybu.gameframe.commands.CommandLoader;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.map.MapService;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.modules.misc.GameSize;
import de.kybu.gameframe.modules.misc.Module;
import de.kybu.quickshot.commands.SkipCommand;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.listener.*;
import de.kybu.quickshot.listener.player.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class QuickShot extends JavaPlugin {

    private static QuickShot instance;

    public static QuickShot getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {

    }

    @Override
    public void onEnable() {
        instance = this;

        Module thisModule = ModuleService.getInstance().getActivatedModule();

        thisModule.setGame(new QuickShotGame());
        thisModule.getGame().setCurrentStatus(GameStatus.LOBBY);

        ModuleService.getInstance().registerAllListenerInPackage("de.kybu.quickshot.listener", this, this.getClassLoader());
        ModuleService.getInstance().registerAllCommandsInPackage("de.kybu.quickshot.commands", this.getClassLoader());

        GameSize gameSize = thisModule.getModuleConfiguration().getAllowedGameSize().get(0);

        QuickShotGame game = (QuickShotGame) thisModule.getGame();
        game.setGameSize(gameSize);
        game.resetGame();
    }

    @Override
    public void onDisable() {
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        game.stopGameTimer();

        GameInventory.INVENTORIES.clear();
    }
}

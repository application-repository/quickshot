package de.kybu.quickshot.shop;

import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.item.Item;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.QuickShot;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.scoreboard.QuickShotScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ShopInventory extends GameInventory{

    private static ShopInventory instance;

    public ShopInventory(){
        super("§8» §eShop", 45, "quickshot-shop-default");
        this.initializeDefaults();
        this.registerClickListener(QuickShot.getInstance());
    }

    private void initializeDefaults(){
        this.getInventory().clear();

        this.setHolder(0);
        this.setHolder(4);

        Inventory inventory = this.getInventory();
        inventory.setItem(10, new Item(Material.IRON_SWORD).setDisplayName("§8» §eSchwerter").build());
        inventory.setItem(16, new Item(Material.IRON_CHESTPLATE).setDisplayName("§8» §eRüstung").build());
        inventory.setItem(13, new Item(Material.COOKED_BEEF).setDisplayName("§8» §eEssen").build());
    }

    public Inventory createCopy(){
        Inventory inventory = Bukkit.createInventory(null, 45, "§8» §eShop");
        inventory.setContents(this.getInventory().getContents());
        return inventory;
    }

    @Override
    public void handle(InventoryClickEvent event) {
        final Player player = (Player) event.getWhoClicked();
        event.setCancelled(true);

        if(event.getCurrentItem().getItemMeta().hasDisplayName() && event.getCurrentItem().getItemMeta().getDisplayName().startsWith("§8»")){
            event.getInventory().setContents(this.getInventory().getContents());
            switch (event.getCurrentItem().getType()){
                case IRON_SWORD:
                    eShopSwordItems.setItems(event.getInventory());
                    break;
                case IRON_CHESTPLATE:
                    eShopArmorItems.setItems(event.getInventory());
                    break;
                case COOKED_BEEF:
                    eShopFoodItems.setItems(event.getInventory());
                    break;
            }
        } else if(event.getCurrentItem().getItemMeta().hasLore()) {

            String loreLine = event.getCurrentItem().getItemMeta().getLore().get(1);
            loreLine = loreLine.replace("§8» §e", "");
            loreLine = loreLine.replace(" §7Level", "");

            int levelNeeded = Integer.parseInt(loreLine);
            if(player.getLevel() >= levelNeeded){
                player.getInventory().addItem(new ItemStack(event.getCurrentItem().getType()));
                player.setLevel(player.getLevel() - levelNeeded);

                QuickShotGame quickShotGame = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
                if(quickShotGame.getQuickShotState() == QuickShotGame.QuickShotState.SHOP)
                    QuickShotScoreboard.getInstance().updateGameScoreboard(QuickShotScoreboard.QuickShotSortType.LEVEL);

            }
        }
    }

    public static ShopInventory getInstance() {
        return (instance != null ? instance : (instance = new ShopInventory()));
    }
}

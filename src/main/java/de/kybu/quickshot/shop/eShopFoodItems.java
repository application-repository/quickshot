package de.kybu.quickshot.shop;

import de.kybu.gameframe.item.Item;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

public enum eShopFoodItems {

    APPLE(Material.APPLE, 2, 29),
    STEAK(Material.COOKED_BEEF, 5, 31),
    GOLD_APPLE(Material.GOLDEN_APPLE, 25, 33);

    private final Material material;
    private final int levelNeeded;
    private final int slot;

    eShopFoodItems(final Material material, final int levelNeeded, final int slot){
        this.material = material;
        this.levelNeeded = levelNeeded;
        this.slot = slot;
    }

    public Material getMaterial() {
        return material;
    }

    public int getLevelNeeded() {
        return levelNeeded;
    }

    public int getSlot() {
        return slot;
    }

    public static void setItems(Inventory inventory){
        for(eShopFoodItems items : eShopFoodItems.values()){
            inventory.setItem(items.getSlot(), new Item(items.material)
                    .addLoreLine(" ")
                    .addLoreLine("§8» §e" + items.getLevelNeeded() + " §7Level")
                    .addLoreLine(" ")
                    .build());
        }
    }

}

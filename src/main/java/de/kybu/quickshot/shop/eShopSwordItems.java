package de.kybu.quickshot.shop;

import de.kybu.gameframe.item.Item;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

public enum eShopSwordItems {

    STONE(Material.STONE_SWORD, 15, 29),
    IRON(Material.IRON_SWORD, 45, 31),
    DIAMOND(Material.DIAMOND_SWORD, 70, 33),
    ROD(Material.FISHING_ROD, 35, 40);


    private final Material material;
    private final int levelNeeded;
    private final int slot;

    eShopSwordItems(final Material material, final int levelNeeded, final int slot){
        this.material = material;
        this.levelNeeded = levelNeeded;
        this.slot = slot;
    }

    public Material getMaterial() {
        return material;
    }

    public int getLevelNeeded() {
        return levelNeeded;
    }

    public int getSlot() {
        return slot;
    }

    public static void setItems(Inventory inventory){
        for(eShopSwordItems items : eShopSwordItems.values()){
            inventory.setItem(items.getSlot(), new Item(items.material)
                    .addLoreLine(" ")
                    .addLoreLine("§8» §e" + items.getLevelNeeded() + " §7Level")
                    .addLoreLine(" ")
                    .build());
        }
    }
}

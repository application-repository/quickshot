package de.kybu.quickshot.shop;

import de.kybu.gameframe.item.Item;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

public enum eShopArmorItems {

    IRON_HELMET(Material.IRON_HELMET, 15, 28),
    IRON_CHESTPLATE(Material.IRON_CHESTPLATE, 45, 30),
    IRON_LEGGINGS(Material.IRON_LEGGINGS, 30, 32),
    IRON_BOOTS(Material.IRON_BOOTS, 15, 34),

    DIAMOND_HELMET(Material.DIAMOND_HELMET, 30, 37),
    DIAMOND_CHESTPLATE(Material.DIAMOND_CHESTPLATE, 90, 39),
    DIAMOND_LEGGINGS(Material.DIAMOND_LEGGINGS, 60, 41),
    DIAMOND_BOOTS(Material.DIAMOND_BOOTS, 30, 43);

    private final Material material;
    private final int levelNeeded;
    private final int slot;

    eShopArmorItems(final Material material, final int levelNeeded, final int slot){
        this.material = material;
        this.levelNeeded = levelNeeded;
        this.slot = slot;
    }

    public Material getMaterial() {
        return material;
    }

    public int getLevelNeeded() {
        return levelNeeded;
    }

    public int getSlot() {
        return slot;
    }

    public static void setItems(Inventory inventory){
        for(eShopArmorItems items : eShopArmorItems.values()){
            inventory.setItem(items.getSlot(), new Item(items.getMaterial())
                    .addLoreLine(" ")
                    .addLoreLine("§8» §e" + items.getLevelNeeded() + " §7Level")
                    .addLoreLine(" ")
                    .build());
        }
    }

}

package de.kybu.quickshot.commands;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@de.kybu.gameframe.commands.Command(name = "skip")
public class SkipCommand extends Command {

    private final List<UUID> votedPlayers;

    public SkipCommand() {
        super("skip");
        this.votedPlayers = new ArrayList<>();
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] args) {

        final Player player = (Player) commandSender;
        final User user = UserService.getInstance().getUser(player.getUniqueId());
        final QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(!game.isPlayer(player))
            return false;

        if(game.getQuickShotState() != QuickShotGame.QuickShotState.SHOP) {
            if(!player.hasPermission("gameframe.skip")){
                user.sendTranslatedMessage("nothing-to-skip", ModuleService.getInstance().getPrefix());
            } else {
                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("dm"))
                        game.setDeathmatchTime(0);
                }
            }
            return false;
        }

        if(this.votedPlayers.contains(player.getUniqueId())){
            user.sendTranslatedMessage("already-voted-for-skip", ModuleService.getInstance().getPrefix());
            return false;
        }

        this.votedPlayers.add(player.getUniqueId());

        Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(player.getUniqueId()), new FutureCallback<IAchievementPlayer>() {
            @Override
            public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                if(!iAchievementPlayer.hasAchievement(7))
                    iAchievementPlayer.unlockAchievement(7 /* Ungeduldig Achievement */);
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        }, GameFrame.getInstance().getExecutorService());

        BukkitUtil.broadcastTranslatedMessage("player-skipped", ModuleService.getInstance().getPrefix(), player.getName(), this.votedPlayers.size() + "§8/§e" + game.getPlayers().size());

        if(skip(game)){
            BukkitUtil.broadcastTranslatedMessage("shop-phase-skipped", ModuleService.getInstance().getPrefix());
            game.getCurrentCountdown().setCurrentSeconds(5);
            this.votedPlayers.clear();
        }

        return false;
    }

    private boolean skip(QuickShotGame game){
        return this.votedPlayers.size() == game.getPlayers().size();
    }
}

package de.kybu.quickshot.countdowns;

import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.items.eQuickShotItems;
import de.kybu.quickshot.game.player.QuickShotPlayer;
import de.kybu.quickshot.game.scoreboard.QuickShotScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.github.paperspigot.Title;

public class PrepareCountdown extends Countdown {

    public PrepareCountdown() {
        super(3);
    }

    @Override
    public void onFinish() {
        Title title = Title.builder()
                .title("§a§lGo")
                .fadeIn(0)
                .stay(10)
                .fadeOut(10)
                .build();

        QuickShotScoreboard.getInstance().updateGameScoreboard(QuickShotScoreboard.QuickShotSortType.LEVEL);
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();

        for(QuickShotPlayer qsPlayer : game.getPlayers().values()){
            Player player = qsPlayer.getPlayer();
            player.playSound(player.getLocation(), Sound.NOTE_BASS, 3, 2);
            player.sendTitle(title);
            player.getInventory().clear();
            eQuickShotItems.setFightItems(player);
            player.setAllowFlight(true);
        }

        for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
            QuickShotScoreboard.getInstance().setGameScoreboard(onlinePlayer);
        }

        game.setQuickShotState(QuickShotGame.QuickShotState.COLLECT);
        game.setCurrentSeconds(0);
        game.startGameTimer();
        game.setStartedAt(System.currentTimeMillis());
    }

    @Override
    public void onTick() {
        Title title = null;
        switch (getCurrentSeconds()){
            case 3:
                title = Title.builder().title("§c§l3").fadeIn(0).stay(10).fadeOut(10).build();
                break;
            case 2:
                title = Title.builder().title("§6§l2").fadeIn(0).stay(10).fadeOut(10).build();
                break;
            case 1:
                title = Title.builder().title("§e§l1").fadeIn(0).stay(10).fadeOut(10).build();
                break;
        }

        for(Player player : Bukkit.getOnlinePlayers()){
            player.playSound(player.getLocation(), Sound.NOTE_BASS, 3, 1);
            player.sendTitle(title);
        }
    }

}

package de.kybu.quickshot.countdowns;

import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import org.bukkit.Bukkit;
import org.bukkit.Sound;

public class LobbyCountdown extends Countdown {

    public LobbyCountdown() {
        super(60);
    }

    @Override
    public void onFinish() {
        ModuleService.getInstance().getActivatedModule().getGame().startGame();
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.playSound(player.getLocation(), Sound.NOTE_PLING, 3, 1);
            player.setLevel(0);
        });
        BukkitUtil.broadcastTranslatedMessage("game-starts-now", ModuleService.getInstance().getPrefix());
    }

    @Override
    public void onTick() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.setLevel(getCurrentSeconds());
            if(getCurrentSeconds() == 15 || getCurrentSeconds() == 30 || getCurrentSeconds() == 10 || getCurrentSeconds() <= 5){
                player.playSound(player.getLocation(), Sound.NOTE_BASS, 3, 1);
            }
        });
        if(getCurrentSeconds() == 15 || getCurrentSeconds() == 30 || getCurrentSeconds() == 10 || getCurrentSeconds() <= 5) {
            BukkitUtil.broadcastTranslatedMessage("game-starts-in", ModuleService.getInstance().getPrefix(), getCurrentSeconds() + "");

        }
    }
}

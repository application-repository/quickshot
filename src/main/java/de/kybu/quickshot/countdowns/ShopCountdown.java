package de.kybu.quickshot.countdowns;

import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.message.ActionBarMessage;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.player.QuickShotPlayer;
import de.kybu.quickshot.game.scoreboard.QuickShotScoreboard;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Iterator;

public class ShopCountdown extends Countdown {

    public ShopCountdown() {
        super(60);
    }

    @Override
    public void onFinish() {
        QuickShotGame quickShotGame = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        Iterator<Location> locations = quickShotGame.getDeathmatchLocations().iterator();

        for(QuickShotPlayer player : quickShotGame.getPlayers().values()) {

            player.setKillStreak(0);

            BukkitUtil.resetPlayer(player.getPlayer(), true, false);
            player.getPlayer().setAllowFlight(false);

            player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.NOTE_PLING, 3, 1);
        }


        for (Player player : Bukkit.getOnlinePlayers()) {
            if(locations.hasNext()){
                player.getPlayer().teleport(locations.next());
            } else {
                locations = quickShotGame.getDeathmatchLocations().iterator();
                player.getPlayer().teleport(locations.next());
            }
        }

        quickShotGame.setQuickShotState(QuickShotGame.QuickShotState.FIGHT);
        quickShotGame.setCurrentSeconds(0);
        QuickShotScoreboard.getInstance().updateGameScoreboard(QuickShotScoreboard.QuickShotSortType.LIVES);
    }

    @Override
    public void onTick() {
        if(getCurrentSeconds() == 15 || getCurrentSeconds() == 30 || getCurrentSeconds() <= 5){
            BukkitUtil.broadcastTranslatedMessage("deathmatch-starts-in", ModuleService.getInstance().getPrefix(), getCurrentSeconds() + "");
            Bukkit.getOnlinePlayers().forEach(player -> player.playSound(player.getLocation(), Sound.NOTE_BASS, 3, 1));
        }
        UserService.getInstance().getOnlineUsers().forEach(user -> {
            ActionBarMessage actionBarMessage = new ActionBarMessage(user.getPlayerLanguage().getTranslation("quickshot-time-left-deathmatch", getCurrentSeconds() + ""));
            user.sendMessage(actionBarMessage);
        });
    }


}

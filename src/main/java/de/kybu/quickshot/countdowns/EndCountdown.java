package de.kybu.quickshot.countdowns;

import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import org.bukkit.Bukkit;

public class EndCountdown extends Countdown {

    public EndCountdown() {
        super(10);
    }

    @Override
    public void onFinish() {
        ModuleService.getInstance().getActivatedModule().getGame().resetGame();
        Bukkit.getOnlinePlayers().forEach(player -> player.setLevel(0));
    }

    @Override
    public void onTick() {
        BukkitUtil.broadcastTranslatedMessage("game-stops-in", ModuleService.getInstance().getPrefix(), getCurrentSeconds() + "");
        Bukkit.getOnlinePlayers().forEach(player -> {
            player.setLevel(getCurrentSeconds());
            player.setAllowFlight(false);
            player.setFlying(false);
        });
    }

}

package de.kybu.quickshot.listener;

import de.kybu.gameframe.events.GameFrameMapLoadEvent;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class GameFrameMapLoadHandler implements Listener {

    @EventHandler
    public void handle(final GameFrameMapLoadEvent event){
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        game.clearSpawnLocation();

        event.getMap().getMapLocations().forEach((s, xyzw) -> {
            if(s.startsWith("loc_"))
                game.addSpawnLocation(xyzw.toLocation());

            if(s.startsWith("dm_"))
                game.addDeathmatchLocation(xyzw.toLocation());
        });
    }

}

package de.kybu.quickshot.listener.player;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.CombatLog;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PlayerDamageByPlayerHandler implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(final EntityDamageByEntityEvent event){

        if(event.getEntity().getType() != EntityType.PLAYER)
            return;

        Player player = (Player) event.getEntity();
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(game.getQuickShotState() == QuickShotGame.QuickShotState.COLLECT || game.getQuickShotState() == QuickShotGame.QuickShotState.FIGHT){
            if(!game.isPlayer(player)){
                event.setDamage(0);
                event.setCancelled(true);
                return;
            }

            Player damager = this.getDamager(event.getDamager());
            if(damager == null)
                return;

            if(player.equals(damager)){
                event.setCancelled(true);
                return;
            }


            if(!game.isPlayer(damager)){
                event.setDamage(0);
                event.setCancelled(true);
                return;
            }

            if(game.hasSpawnProtection((Player) event.getEntity())){
                event.setCancelled(true);
                return;
            }

            if(damager.getItemInHand().getType() == Material.AIR && game.getQuickShotState() == QuickShotGame.QuickShotState.COLLECT){
                event.setDamage(0);
                return;
            }

            if(CombatLog.getInstance().isInCombat(player))
                CombatLog.getInstance().removePlayer(player);

            CombatLog.getInstance().addFightingPlayers(player, damager);
        }
    }

    private Player getDamager(Entity entity){

        if(entity.getType() == EntityType.ARROW){
            Arrow arrow = (Arrow) entity;
            if(!(arrow.getShooter() instanceof Player))
                return null;

            return (Player) arrow.getShooter();
         } else if(entity.getType() == EntityType.PLAYER){
            return (Player) entity;
        }

        return null;

    }

}

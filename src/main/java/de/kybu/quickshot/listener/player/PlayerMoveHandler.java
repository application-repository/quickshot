package de.kybu.quickshot.listener.player;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveHandler implements Listener {

    @EventHandler
    public void handle(final PlayerMoveEvent event){
        final Player player = event.getPlayer();

        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(game.getQuickShotState() == QuickShotGame.QuickShotState.PREPARE){
            if(event.getFrom().getZ() != event.getTo().getZ() || event.getFrom().getX() != event.getTo().getX())
                player.teleport(event.getFrom());
        }
    }

}

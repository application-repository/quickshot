package de.kybu.quickshot.listener.player;

import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageHandler implements Listener {

    @EventHandler
    public void handle(final EntityDamageEvent event){

        if(event.getCause() == EntityDamageEvent.DamageCause.DROWNING || event.getCause() == EntityDamageEvent.DamageCause.FALL){
            event.setCancelled(true);
            return;
        }

        QuickShotGame quickShotGame = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(!quickShotGame.isPlayer((Player) event.getEntity())){
            event.setCancelled(true);
            return;
        }

        if(quickShotGame.getCurrentStatus() == GameStatus.LOBBY){
            event.setCancelled(true);
            return;
        }

        switch (quickShotGame.getQuickShotState()){
            case NONE:
            case PREPARE:
            case SHOP:
                event.setCancelled(true);
                break;
            case FIGHT:
            case COLLECT:
                event.setCancelled(false);
                break;
        }

    }

}

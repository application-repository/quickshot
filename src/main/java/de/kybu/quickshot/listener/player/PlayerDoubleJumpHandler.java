package de.kybu.quickshot.listener.player;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.QuickShot;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class PlayerDoubleJumpHandler implements Listener {

    private static final double HEIGHT = 2.2;

    private static final double MULTIPLY = 2.2;

    private final QuickShotGame quickShotGame;

    public PlayerDoubleJumpHandler(){
        this.quickShotGame = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(final PlayerToggleFlightEvent event) {
        if(quickShotGame.getQuickShotState() != QuickShotGame.QuickShotState.COLLECT)
            return;

        final Player player = event.getPlayer();
        if (player.getGameMode() != GameMode.CREATIVE && this.quickShotGame.isPlayer(player)) {
            event.setCancelled(true);
            player.setAllowFlight(false);
            player.setExp(0.0f);
            player.setVelocity(player.getLocation().getDirection().multiply(0.5 * MULTIPLY).setY(0.5 * HEIGHT));
            player.playSound(player.getLocation(), Sound.ENDERDRAGON_WINGS, 2.0f, 5.0f);
            Bukkit.getScheduler().scheduleSyncDelayedTask(QuickShot.getInstance(), () -> {
                player.setAllowFlight(true);
            }, 40);
        }
    }

}

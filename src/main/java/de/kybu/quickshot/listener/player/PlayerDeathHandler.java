package de.kybu.quickshot.listener.player;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.CombatLog;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.QuickShot;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.items.eQuickShotItems;
import de.kybu.quickshot.game.player.QuickShotPlayer;
import de.kybu.quickshot.game.scoreboard.QuickShotScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerDeathHandler implements Listener {

    private final QuickShotGame game;

    public PlayerDeathHandler(){
        this.game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler
    public void handle(final PlayerDeathEvent event){

        event.setDeathMessage(null);
        event.getEntity().playSound(event.getEntity().getLocation(), Sound.CREEPER_DEATH, 3, 1);

        QuickShotPlayer quickShotPlayer = game.getPlayers().get(event.getEntity().getUniqueId());
        final User user = UserService.getInstance().getUser(quickShotPlayer.getPlayer().getUniqueId());

        if(game.getQuickShotState() == QuickShotGame.QuickShotState.COLLECT){
            event.setKeepInventory(false);
            event.getDrops().clear();

            Player killer = CombatLog.getInstance().getMap().get(quickShotPlayer.getPlayer());
            if(killer != null){
                final User userKiller = UserService.getInstance().getUser(killer.getUniqueId());
                user.sendTranslatedMessage("bw-killed-by-player", ModuleService.getInstance().getPrefix(), "§c" + killer.getName());
                quickShotPlayer.getPlayer().sendMessage(ModuleService.getInstance().getPrefix() + "§7- §c5 §7Level");
                userKiller.sendTranslatedMessage("bw-you-killed-player", ModuleService.getInstance().getPrefix(), "§a" + quickShotPlayer.getPlayer().getName());
                killer.setLevel(killer.getLevel() + 20);
                killer.sendMessage(ModuleService.getInstance().getPrefix() + "§7+ §a20 §7Level");
                killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 3, 1);
                killer.getInventory().addItem(eQuickShotItems.ARROW.getItemStack());

                QuickShotPlayer killerQS = game.getPlayers().get(killer.getUniqueId());
                if(killerQS != null)
                    killerQS.addKill();

                if(quickShotPlayer.getKillStreak() >= 5){
                    BukkitUtil.broadcastTranslatedMessage("fucked-killstreak", ModuleService.getInstance().getPrefix(), killer.getName(), quickShotPlayer.getPlayer().getName(), quickShotPlayer.getKillStreak() + "");
                    killer.sendMessage(ModuleService.getInstance().getPrefix() + "+ §a30 §7Level");
                    killer.setLevel(killer.getLevel() + 30);
                }
            } else {
                user.sendTranslatedMessage("you-died-lol", ModuleService.getInstance().getPrefix());
                quickShotPlayer.getPlayer().sendMessage(ModuleService.getInstance().getPrefix() + "§7- §c5 &7Level");
            }
            if(quickShotPlayer.getPlayer().getLevel() > 0)
                quickShotPlayer.getPlayer().setLevel(quickShotPlayer.getPlayer().getLevel() - 5);

            QuickShotScoreboard.getInstance().updateGameScoreboard(QuickShotScoreboard.QuickShotSortType.LEVEL);

        } else if(game.getQuickShotState() == QuickShotGame.QuickShotState.FIGHT){

            quickShotPlayer.increaseLives();
            QuickShotScoreboard.getInstance().updateGameScoreboard(QuickShotScoreboard.QuickShotSortType.LIVES);

            Player killer = CombatLog.getInstance().getMap().get(quickShotPlayer.getPlayer());
            if(killer != null){
                BukkitUtil.broadcastTranslatedMessage("bw-bc-player-killed-by-player", ModuleService.getInstance().getPrefix(), quickShotPlayer.getPlayer().getName(), killer.getName());
                killer.setLevel(killer.getLevel() + 20);
                killer.sendMessage(ModuleService.getInstance().getPrefix() + "§7+ §a20 §7Level");
                quickShotPlayer.getPlayer().sendMessage(ModuleService.getInstance().getPrefix() + "§7- §c1 §7Level");
                killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 3, 1);
                killer.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 100, 2, false, true));
                QuickShotPlayer killerQS = game.getPlayers().get(killer.getUniqueId());

                if(killerQS != null)
                    killerQS.addKill();

                if(quickShotPlayer.getKillStreak() >= 5){
                    BukkitUtil.broadcastTranslatedMessage("fucked-killstreak", ModuleService.getInstance().getPrefix(), killer.getName(), quickShotPlayer.getPlayer().getName(), quickShotPlayer.getKillStreak() + "");
                    killer.sendMessage(ModuleService.getInstance().getPrefix() + "+ §a20 §7Level");
                    killer.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE));
                    killer.setLevel(killer.getLevel() + 20);
                }

                if(quickShotPlayer.getPlayer().getLevel() > 0)
                    quickShotPlayer.getPlayer().setLevel(quickShotPlayer.getPlayer().getLevel() - 1);
            } else {
                BukkitUtil.broadcastTranslatedMessage("bw-bc-player-died-no-killer", ModuleService.getInstance().getPrefix(), quickShotPlayer.getPlayer().getName());
                quickShotPlayer.getPlayer().sendMessage(ModuleService.getInstance().getPrefix() + "§7- §c1 §7Level");
                if(quickShotPlayer.getPlayer().getLevel() > 0)
                    quickShotPlayer.getPlayer().setLevel(quickShotPlayer.getPlayer().getLevel() - 1);
            }

            if(quickShotPlayer.getLives() == 0) {
                event.setKeepInventory(false);
                for(ItemStack itemStack : event.getDrops()){
                    if(itemStack.getType() == Material.CHEST){
                        event.getDrops().remove(itemStack);
                        break;
                    }
                }
                Bukkit.getScheduler().runTaskLater(QuickShot.getInstance(), () -> {
                    game.setSpectator(event.getEntity());
                }, 10);
            } else {
                event.setKeepInventory(true);
                event.getDrops().clear();
            }

        }
        quickShotPlayer.setKillStreak(0);
        event.setDroppedExp(0);
        event.setKeepLevel(true);

        game.setSpawnProtection(quickShotPlayer.getPlayer());
        Bukkit.getScheduler().runTaskLater(QuickShot.getInstance(), () -> {
            game.removeSpawnProtection(quickShotPlayer.getPlayer());
            UserService.getInstance().getUser(quickShotPlayer.getPlayer().getUniqueId()).sendTranslatedMessage("your-now-hitable", ModuleService.getInstance().getPrefix());
        }, 40);

        Bukkit.getScheduler().runTaskLater(QuickShot.getInstance(), () -> event.getEntity().spigot().respawn(), 20);

    }

}

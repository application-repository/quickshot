package de.kybu.quickshot.listener.player;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.language.Language;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import javax.annotation.Nullable;

public class PlayerQuitHandler implements Listener {

    @EventHandler
    public void handle(final PlayerQuitEvent event){
        final Player player = event.getPlayer();
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();

        event.setQuitMessage(null);

        switch (game.getCurrentStatus()){
            case LOBBY:
                for (User user : UserService.getInstance().getOnlineUsers()) {
                    user.sendMessage(/* PLAYER RANK COLOR */ "§7« §c" + player.getName() + " §7" + user.getPlayerLanguage().getTranslation("player-left"));
                }
                game.dropPlayer(player.getUniqueId());

                if((!game.isReady()) && (game.getCurrentCountdown() != null)){
                    BukkitUtil.broadcastTranslatedMessage("countdown-aborted", ModuleService.getInstance().getPrefix());
                    game.getCurrentCountdown().stop();
                    game.setCurrentCountdown(null);
                }

                break;
            case RUNNING:

                if(game.isPlayer(player.getUniqueId())){
                    Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(player.getUniqueId()), new FutureCallback<IAchievementPlayer>() {
                        @Override
                        public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                            if(!iAchievementPlayer.hasAchievement(5))
                                iAchievementPlayer.unlockAchievement(5 /* Feigling Achievement */);
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            throwable.printStackTrace();
                        }
                    }, GameFrame.getInstance().getExecutorService());

                    for (User user : UserService.getInstance().getOnlineUsers()) {
                        user.sendMessage(/* PLAYER RANK COLOR */ModuleService.getInstance().getPrefix() + "§c" + player.getName() + " §7" + user.getPlayerLanguage().getTranslation("player-left"));
                    }
                    game.dropPlayer(player.getUniqueId());
                }
                break;
        }
    }

}

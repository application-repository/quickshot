package de.kybu.quickshot.listener.player;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.items.eQuickShotItems;
import de.kybu.quickshot.game.scoreboard.QuickShotScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinHandler implements Listener {

    private final QuickShotGame game;

    public PlayerJoinHandler(){
        this.game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler
    public void handle(final PlayerJoinEvent event){
        Player player = event.getPlayer();
        final User user = UserService.getInstance().getUser(player.getUniqueId());

        if(game.getCurrentStatus() == GameStatus.RUNNING){

            if(game.getQuickShotState() == QuickShotGame.QuickShotState.PREPARE){
                player.kickPlayer("§7Game starts at the moment");
                return;
            }

            user.sendTranslatedMessage("game-already-started", ModuleService.getInstance().getPrefix());
            QuickShotScoreboard.getInstance().setGameScoreboard(player);
            game.setSpectator(player);


        } else if(game.getCurrentStatus() == GameStatus.ENDING){
            player.kickPlayer(user.getPlayerLanguage().getTranslation("round-ending"));
        } else if(game.getCurrentStatus() == GameStatus.LOBBY){

            if(game.isFull()){
                player.kickPlayer(user.getPlayerLanguage().getTranslation("round-full"));
                return;
            }
            BukkitUtil.resetPlayer(player, true, true);
            BukkitUtil.clearInventory(player);

            for (User onlineUser : UserService.getInstance().getOnlineUsers()) {
                onlineUser.sendMessage(/* PLAYER RANK COLOR */"§8» §c" + player.getName() + onlineUser.getPlayerLanguage().getTranslation("player-joined"));
            }
            player.teleport(GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation());
            game.registerPlayer(player);

            QuickShotScoreboard.getInstance().setLobbyScoreboard(player);

            if(game.isReady() && (game.getCurrentCountdown() == null)){
                BukkitUtil.broadcastTranslatedMessage("countdown-started", ModuleService.getInstance().getPrefix());
                game.startPrepare();
            }
            player.setGameMode(GameMode.ADVENTURE);
            eQuickShotItems.setLobbyItems(player);
        }
    }


}

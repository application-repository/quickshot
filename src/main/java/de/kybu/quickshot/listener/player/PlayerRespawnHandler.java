package de.kybu.quickshot.listener.player;

import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.QuickShot;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.items.eQuickShotItems;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.util.Vector;

public class PlayerRespawnHandler implements Listener {

    private final QuickShotGame game;

    public PlayerRespawnHandler(){
        this.game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler
    public void handle(final PlayerRespawnEvent event){

        if(game.getCurrentStatus() == GameStatus.RUNNING){
            if(game.getQuickShotState() == QuickShotGame.QuickShotState.COLLECT){
                event.setRespawnLocation(game.getRandomSpawnLocation());
                event.getPlayer().getInventory().clear();
                event.getPlayer().setVelocity(new Vector(0, 0, 0));
                Bukkit.getScheduler().runTaskLater(QuickShot.getInstance(), () -> {
                    eQuickShotItems.setFightItems(event.getPlayer());
                    event.getPlayer().updateInventory();
                    event.getPlayer().setVelocity(new Vector(0, 0, 0));
                }, 10);
            } else {
                event.setRespawnLocation(game.getRandomDeathmatchLocation());
                event.getPlayer().setVelocity(new Vector(0, 0, 0));
            }
        }

    }

}

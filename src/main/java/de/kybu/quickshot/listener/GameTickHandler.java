package de.kybu.quickshot.listener;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.message.ActionBarMessage;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.QuickShot;
import de.kybu.quickshot.countdowns.ShopCountdown;
import de.kybu.quickshot.events.GameTickEvent;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.items.eQuickShotItems;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.text.SimpleDateFormat;

public class GameTickHandler implements Listener {

    private final SimpleDateFormat simpleDateFormat;

    public GameTickHandler(){
        this.simpleDateFormat = new SimpleDateFormat("mm:ss");
    }

    @EventHandler
    public void handle(final GameTickEvent event){

        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(game.getQuickShotState() == QuickShotGame.QuickShotState.COLLECT){
            UserService.getInstance().getOnlineUsers().forEach(user -> {
                ActionBarMessage actionBarMessage = new ActionBarMessage(user.getPlayerLanguage().getTranslation("quickshot-time-left-shop", simpleDateFormat.format(game.getDeathmatchTime() - System.currentTimeMillis())));
                user.sendMessage(actionBarMessage);
            });

            if(game.getDeathmatchTime() - System.currentTimeMillis() <= 0){
                game.setQuickShotState(QuickShotGame.QuickShotState.SHOP);
                UserService.getInstance().getOnlineUsers().forEach(user -> {
                    ActionBarMessage actionBarMessage = new ActionBarMessage(user.getPlayerLanguage().getTranslation("quickshot-shop-starting-now", simpleDateFormat.format(game.getDeathmatchTime() - System.currentTimeMillis())));
                    user.sendMessage(actionBarMessage);
                });
                Location location = GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation();

                Bukkit.getScheduler().runTask(QuickShot.getInstance(), () -> {
                    Countdown countdown = new ShopCountdown();
                    countdown.start();
                    game.setCurrentCountdown(countdown);

                    for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                        onlinePlayer.teleport(location);
                        onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.NOTE_PLING, 3, 2);
                    }

                    Bukkit.getScheduler().runTaskLater(QuickShot.getInstance(), () -> {
                        game.getPlayers().forEach((uuid, quickShotPlayer) -> {
                            BukkitUtil.resetPlayer(quickShotPlayer.getPlayer(), true, false);
                            BukkitUtil.clearInventory(quickShotPlayer.getPlayer());
                            eQuickShotItems.setShopItem(quickShotPlayer.getPlayer());
                            quickShotPlayer.getPlayer().updateInventory();
                        });
                    }, 5);
                });

            }
        } else if (game.getQuickShotState() == QuickShotGame.QuickShotState.FIGHT){

            if(event.getTickSeconds() % 5 == 0)
                game.getPlayers().values()
                .stream()
                .filter(quickShotPlayer -> quickShotPlayer.isAlive())
                        .forEach((quickShotPlayer) -> quickShotPlayer.getPlayer().setLevel(quickShotPlayer.getPlayer().getLevel() + 1));

        }

    }

}

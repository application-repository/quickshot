package de.kybu.quickshot.listener;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;

public class InventoryClickHandler implements Listener {

    private final QuickShotGame quickShotGame;

    public InventoryClickHandler(){
        this.quickShotGame = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
    }

    @EventHandler
    public void handle(final InventoryClickEvent event){

        if(this.quickShotGame.getQuickShotState() == QuickShotGame.QuickShotState.NONE || this.quickShotGame.getQuickShotState() == QuickShotGame.QuickShotState.COLLECT || this.quickShotGame.getQuickShotState() == QuickShotGame.QuickShotState.PREPARE )
            event.setCancelled(true);

    }

    @EventHandler
    public void handle(final InventoryMoveItemEvent event){
        if(event.getItem().hasItemMeta()){
            if(event.getItem().getItemMeta().hasDisplayName()){
                String dpName = event.getItem().getItemMeta().getDisplayName();
                if(dpName.equalsIgnoreCase("§eShop")){
                    event.setCancelled(true);
                }
            }
        }
    }

}

package de.kybu.quickshot.listener;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class ChatHandler implements Listener {

    @EventHandler
    public void handle(final AsyncPlayerChatEvent event){
        final Player player = event.getPlayer();

        QuickShotGame quickShotGame = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(event.getMessage().contains("%"))
            event.setMessage(event.getMessage().replace("%", ""));

        if(!quickShotGame.isPlayer(player)){
            event.setCancelled(true);

            for (User onlineUser : UserService.getInstance().getOnlineUsers()) {
                if(!quickShotGame.isPlayer(onlineUser.getUuid())){
                    onlineUser.sendMessage("§7" + player.getName() + "§7: §f" + event.getMessage());
                }
            }
        } else {
            event.setFormat(/* PLAYER RANK COLOR */"§c" + player.getName() + "§7: §f" + event.getMessage());
        }


    }

}

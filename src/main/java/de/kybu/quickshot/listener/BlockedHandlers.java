package de.kybu.quickshot.listener;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.quickshot.game.QuickShotGame;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class BlockedHandlers implements Listener {

    @EventHandler
    public void handle(final PlayerDropItemEvent event) {
        if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
            event.setCancelled(true);
    }

    @EventHandler
    public void handle(final BlockBreakEvent event) {
        if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
            event.setCancelled(true);
    }

    @EventHandler
    public void handle(final BlockPlaceEvent event) {
        if (event.getPlayer().getGameMode() != GameMode.CREATIVE)
            event.setCancelled(true);
    }

    @EventHandler
    public void handle(final FoodLevelChangeEvent event) {
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();

        if(!game.isPlayer((Player) event.getEntity()))
            event.setCancelled(true);

        if (game.getQuickShotState() != QuickShotGame.QuickShotState.FIGHT)
            event.setCancelled(true);
    }

    @EventHandler
    public void handle(final PlayerPickupItemEvent event){
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(!game.isPlayer(event.getPlayer()))
            event.setCancelled(true);
    }

    @EventHandler
    public void handle(final ProjectileHitEvent event){
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();

        if (game.getQuickShotState() != QuickShotGame.QuickShotState.FIGHT)
            event.getEntity().remove();
    }

    @EventHandler
    public void handle(final HangingBreakEvent event) {
        event.setCancelled(true);
    }

    @EventHandler
    public void handle(final PlayerInteractEvent event){
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(!game.isPlayer(event.getPlayer())){
            event.setCancelled(true);
            return;
        }
        if(event.getClickedBlock() != null){
            event.setCancelled(true);
        }
    }

}

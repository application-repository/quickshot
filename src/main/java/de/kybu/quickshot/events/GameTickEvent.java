package de.kybu.quickshot.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameTickEvent extends Event {

    public static HandlerList HANDLERS = new HandlerList();

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    private final int tickSeconds;

    public GameTickEvent(final int tickSeconds){
        this.tickSeconds = tickSeconds;
    }

    public int getTickSeconds() {
        return tickSeconds;
    }
}
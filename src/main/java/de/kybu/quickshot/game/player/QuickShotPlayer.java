package de.kybu.quickshot.game.player;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.quickshot.game.QuickShotGame;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;

@Getter
@Setter
public class QuickShotPlayer {

    private final Player player;
    private int killStreak;
    private int lives;
    private int kills;
    private boolean isAlive;

    public QuickShotPlayer(Player player, int killStreak, int lives){
        this.player = player;
        this.killStreak = killStreak;
        this.lives = lives;
        this.kills = 0;
        this.isAlive = true;
    }

    public void increaseLives(){
        this.lives = lives - 1;
        if(this.lives == 0){
            BukkitUtil.broadcastTranslatedMessage("quickshot-player-final-death", ModuleService.getInstance().getPrefix(), player.getName());
            this.isAlive = false;
            QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
            game.updateSpectatorInventory();
            game.checkForWinnnerAndInit();
        }
    }

    public void addKill(){
        this.killStreak = this.killStreak + 1;

        if(this.killStreak % 5 == 0){
            Futures.addCallback(IAchievementPlayerProvider.getInstance().getAchievementPlayer(player.getUniqueId()), new FutureCallback<IAchievementPlayer>() {
                @Override
                public void onSuccess(@Nullable IAchievementPlayer iAchievementPlayer) {
                    if(!iAchievementPlayer.hasAchievement(6))
                        iAchievementPlayer.unlockAchievement(6 /* Killstreak Achievement */);
                }

                @Override
                public void onFailure(Throwable throwable) {
                    throwable.printStackTrace();
                }
            }, GameFrame.getInstance().getExecutorService());

            BukkitUtil.broadcastTranslatedMessage("quickshot-killstreak-announcement", ModuleService.getInstance().getPrefix(), player.getName(), this.killStreak + "");
            player.sendMessage(ModuleService.getInstance().getPrefix() + "§7+ §a30 §7Level");
            player.setLevel(player.getLevel() + 30);
            for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
                onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.LEVEL_UP, 3, 1);
            }
        }
    }

    public boolean isAlive() {
        return isAlive;
    }
}

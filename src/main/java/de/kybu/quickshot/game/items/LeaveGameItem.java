package de.kybu.quickshot.game.items;

import de.kybu.gameframe.item.ClickableItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class LeaveGameItem extends ClickableItem {

    public LeaveGameItem() {
        super(Material.WATCH, 1, (short) 0);
        this.setDisplayName("§cSpiel verlassen");
    }

    @Override
    public void handle(Player player) {
        player.kickPlayer("");
    }
}

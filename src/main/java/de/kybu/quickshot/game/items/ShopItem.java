package de.kybu.quickshot.game.items;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.item.ClickableItem;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.shop.ShopInventory;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import javax.annotation.Nullable;
import java.util.concurrent.Future;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 06.02.2021
 */
public class ShopItem extends ClickableItem {

    public ShopItem() {
        super(Material.CHEST, 1, (short) 0);
        this.setDisplayName("§eShop");
    }

    @Override
    public void handle(Player player) {
        User user = UserService.getInstance().getUser(player.getUniqueId());
        if(!user.canExecute()){
            user.sendTranslatedMessage("rate-limit-exceeded");
            return;
        }

        Futures.addCallback(getShopInventory(), new FutureCallback<Inventory>() {
            @Override
            public void onSuccess(@Nullable Inventory shopInventory) {
                player.openInventory(shopInventory);
            }

            @Override
            public void onFailure(Throwable throwable) {
                throwable.printStackTrace();
            }
        }, GameFrame.getInstance().getExecutorService());
    }

    private ListenableFuture<Inventory> getShopInventory(){
        SettableFuture<Inventory> future = SettableFuture.create();
        GameFrame.getInstance().getExecutorService().execute(() -> {
            future.set(ShopInventory.getInstance().createCopy());
        });

        return future;
    }
}

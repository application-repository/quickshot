package de.kybu.quickshot.game.items;

import de.kybu.gameframe.item.Item;
import de.kybu.gameframe.item.items.AchievementsItem;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public enum eQuickShotItems {

    SWORD(new Item(Material.IRON_SWORD).setDisplayName("§eSchwert").setUnbreakable(true).build(), 0),
    BOW(new Item(Material.BOW).setDisplayName("§eBogen").setUnbreakable(true).build(), 1),
    LEAVE(new LeaveGameItem().build(), 0),
    ARROW(new Item(Material.ARROW).setDisplayName("§ePfeil").build(), 8),
    SHOP(new ShopItem().build(), 8);

    private final ItemStack itemStack;
    private final int itemSlot;

    eQuickShotItems(final ItemStack itemStack, final int itemSlot){
        this.itemStack = itemStack;
        this.itemSlot = itemSlot;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public int getItemSlot() {
        return itemSlot;
    }

    public static void setFightItems(final Player player){
        player.getInventory().setItem(SWORD.getItemSlot(), SWORD.getItemStack());
        player.getInventory().setItem(BOW.getItemSlot(), BOW.getItemStack());
        player.getInventory().setItem(ARROW.getItemSlot(), ARROW.getItemStack());
    }

    public static void setShopItem(final Player player){
        player.getInventory().setItem(SHOP.getItemSlot(), SHOP.getItemStack());
    }

    public static void setLobbyItems(final Player player){
        player.getInventory().setItem(8, AchievementsItem.getInstance().build());
        player.getInventory().setItem(LEAVE.getItemSlot(), LEAVE.getItemStack());
    }
}

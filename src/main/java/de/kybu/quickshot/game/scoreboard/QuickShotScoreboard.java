package de.kybu.quickshot.game.scoreboard;

import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.scoreboard.GameBoard;
import de.kybu.quickshot.game.QuickShotGame;
import de.kybu.quickshot.game.player.QuickShotPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class QuickShotScoreboard {

    private static QuickShotScoreboard instance;

    private final Scoreboard globalGameBoard;
    private final Scoreboard globalLobbyScoreboard;

    public QuickShotScoreboard(){
        this.globalGameBoard = Bukkit.getScoreboardManager().getNewScoreboard();
        this.globalLobbyScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
    }

    public void setLobbyScoreboard(final Player player){
        GameBoard gameBoard = new GameBoard(this.globalLobbyScoreboard);
        gameBoard.set(2, "§f");
        gameBoard.set(1, "§fSpieler§8:");
        gameBoard.set(0, "§a" + ((QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame()).getPlayers().size());
        gameBoard.display();

        if(this.globalLobbyScoreboard.getTeam("0001dev") == null){
            Team team = this.globalLobbyScoreboard.registerNewTeam("0001dev");
            team.setPrefix("§cDev §7| §c");
        }

        this.globalLobbyScoreboard.getTeam("0001dev").addEntry(player.getName());

        player.setScoreboard(this.globalLobbyScoreboard);
    }

    public void setGameScoreboard(final Player player){
        player.setScoreboard(globalGameBoard);

        Scoreboard scoreboard = this.globalGameBoard;
        if(scoreboard.getTeam("0001spectator") == null){
            Team team = scoreboard.registerNewTeam("0001spectator");
            team.setPrefix("§7");
            team.setCanSeeFriendlyInvisibles(true);
        }

        if(scoreboard.getTeam("0002player") == null){
            Team team = scoreboard.registerNewTeam("0002player");
            team.setPrefix("§8» §f");
            team.setCanSeeFriendlyInvisibles(true);
        }

        if(scoreboard.getEntryTeam(player.getName()) != null)
            scoreboard.getEntryTeam(player.getName()).removeEntry(player.getName());

        QuickShotGame quickShotGame = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(quickShotGame.isPlayer(player))
            scoreboard.getTeam("0002player").addEntry(player.getName());
        else
            scoreboard.getTeam("0001spectator").addEntry(player.getName());
    }

    public void updateGameScoreboard(QuickShotSortType type){
        QuickShotGame game = (QuickShotGame) ModuleService.getInstance().getActivatedModule().getGame();
        if(this.globalGameBoard.getObjective(DisplaySlot.SIDEBAR) != null)
            this.globalGameBoard.getObjective(DisplaySlot.SIDEBAR).unregister();

        Map<String, Integer> map = new ConcurrentHashMap<>();

        for(QuickShotPlayer players : game.getPlayers().values()){

            if(!players.isAlive())
                continue;

            int value = 0;
            if(type == QuickShotSortType.LEVEL)
                value = players.getPlayer().getLevel();
            else
                value = players.getLives();

            map.put(players.getPlayer().getName(), value);

        }

        int startIndex = map.size();

        List<Map.Entry<String, Integer>> list = new ArrayList<>(this.entriesSortedByValues(map));
        Collections.reverse(list);

        GameBoard gameBoard = new GameBoard(this.globalGameBoard);
        gameBoard.display();
        gameBoard.set(startIndex + 1, "§r");
        //gameBoard.remove(startIndex + 2);

        for(Map.Entry<String, Integer> entry : list){
            gameBoard.remove(startIndex);
            gameBoard.set(startIndex, "                    ");
            gameBoard.set(startIndex, "§e§l" + entry.getValue() + " §7" + entry.getKey());
            startIndex--;
        }
    }

    public void setSpectator(final Player player){
        Scoreboard scoreboard = this.globalGameBoard;
        if(scoreboard.getTeam("0001spectator") == null){
            Team team = scoreboard.registerNewTeam("0001spectator");
            team.setPrefix("§7");
        }

        if(scoreboard.getEntryTeam(player.getName()) != null)
            scoreboard.getEntryTeam(player.getName()).removeEntry(player.getName());

        scoreboard.getTeam("0001spectator").addEntry(player.getName());
    }

    public static QuickShotScoreboard getInstance() {
        return (instance != null ? instance : (instance = new QuickShotScoreboard()));
    }

    public enum QuickShotSortType {
        LEVEL,
        LIVES
    }

    private <K,V extends Comparable<? super V>> SortedSet<Map.Entry<K,V>> entriesSortedByValues(Map<K,V> map) {
        SortedSet<Map.Entry<K,V>> sortedEntries = new TreeSet<Map.Entry<K,V>>(
                new Comparator<Map.Entry<K,V>>() {
                    @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                        int res = e1.getValue().compareTo(e2.getValue());
                        return res != 0 ? res : 1;
                    }
                }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }
}

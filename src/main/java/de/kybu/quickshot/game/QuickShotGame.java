package de.kybu.quickshot.game;

import de.kybu.gameframe.GameFrame;
import de.kybu.gameframe.game.Game;
import de.kybu.gameframe.game.GameStatus;
import de.kybu.gameframe.game.countdown.Countdown;
import de.kybu.gameframe.game.inventory.GameInventory;
import de.kybu.gameframe.game.inventory.inventories.SpectatorInventory;
import de.kybu.gameframe.item.items.AchievementsItem;
import de.kybu.gameframe.item.items.SpectatorTeleporterItem;
import de.kybu.gameframe.map.MapService;
import de.kybu.gameframe.modules.ModuleService;
import de.kybu.gameframe.util.BukkitUtil;
import de.kybu.gameframe.util.player.User;
import de.kybu.gameframe.util.player.UserService;
import de.kybu.quickshot.QuickShot;
import de.kybu.quickshot.countdowns.EndCountdown;
import de.kybu.quickshot.countdowns.LobbyCountdown;
import de.kybu.quickshot.countdowns.PrepareCountdown;
import de.kybu.quickshot.events.GameTickEvent;
import de.kybu.quickshot.game.items.eQuickShotItems;
import de.kybu.quickshot.game.player.QuickShotPlayer;
import de.kybu.quickshot.game.scoreboard.QuickShotScoreboard;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.github.paperspigot.Title;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Setter
public class QuickShotGame extends Game {

    private final List<Location> spawnLocations;
    private final List<Location> deathmatchLocations;
    private final List<Integer> spawnProtection;
    private final Map<UUID, QuickShotPlayer> players;
    private QuickShotState quickShotState;
    private Timer timer;
    private int currentSeconds;

    private long deathmatchTime;

    public QuickShotGame() {
        super(true);

        this.spawnLocations = new ArrayList<>();
        this.deathmatchLocations = new ArrayList<>();
        this.spawnProtection = new ArrayList<>();

        this.players = new ConcurrentHashMap<>();
        this.quickShotState = QuickShotState.NONE;
        this.currentSeconds = 0;
    }

    @Override
    public void startPrepare() {
        Countdown countdown = new LobbyCountdown();
        countdown.start();

        this.setCurrentCountdown(countdown);
    }

    @Override
    public void startGame() {
        this.setCurrentStatus(GameStatus.RUNNING);
        this.setQuickShotState(QuickShotState.PREPARE);

        AtomicReference<Iterator<Location>> atomicReference = new AtomicReference<>();
        atomicReference.set(this.spawnLocations.iterator());

        GameFrame.getInstance().getExecutorService().execute(() -> Bukkit.getOnlinePlayers().forEach(player -> {
            if(atomicReference.get().hasNext())
                player.teleport(atomicReference.get().next());
            else {
                atomicReference.set(this.spawnLocations.iterator());
                player.teleport(atomicReference.get().next());
            }
        }));

        for(Player player : Bukkit.getOnlinePlayers()){
            BukkitUtil.resetPlayer(player, true, true);
            BukkitUtil.clearInventory(player);
            player.setMaxHealth(2);
            player.setGameMode(GameMode.ADVENTURE);
        }
        this.updateSpectatorInventory();

        Countdown countdown = new PrepareCountdown();
        countdown.start();
        this.setCurrentCountdown(countdown);
    }

    @Override
    public void stopGame() {
        this.setCurrentStatus(GameStatus.ENDING);

        Countdown countdown = new EndCountdown();
        countdown.start();
        this.setCurrentCountdown(countdown);
    }

    @Override
    public void resetGame() {
        this.stopGameTimer();
        GameInventory.INVENTORIES.clear();
        this.spawnProtection.clear();

        AchievementsItem.getInstance().getAchievementsInventoryMap().clear();

        this.setCurrentStatus(GameStatus.LOBBY);
        this.setQuickShotState(QuickShotState.NONE);

        if(this.getCurrentCountdown() != null)
            this.setCurrentCountdown(null);

        this.spawnLocations.clear();
        this.deathmatchLocations.clear();
        this.players.clear();

        for(Player player : Bukkit.getOnlinePlayers()){
            if(this.isFull()){
                player.kickPlayer(UserService.getInstance().getUser(player.getUniqueId()).getPlayerLanguage().getTranslation("round-full"));
                return;
            }

            BukkitUtil.resetPlayer(player, true, true);
            BukkitUtil.clearInventory(player);
            BukkitUtil.showAllHiddenPlayers(player);
            player.setGameMode(GameMode.ADVENTURE);

            for (User onlineUser : UserService.getInstance().getOnlineUsers()) {
                onlineUser.sendMessage(/* PLAYER RANK COLOR */"§8» §c" + player.getName() + onlineUser.getPlayerLanguage().getTranslation("player-joined"));
            }
            player.teleport(GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation());
            this.registerPlayer(player);
            QuickShotScoreboard.getInstance().setLobbyScoreboard(player);
            eQuickShotItems.setLobbyItems(player);

            if(this.isReady() && (this.getCurrentCountdown() == null)){
                BukkitUtil.broadcastTranslatedMessage("countdown-started", ModuleService.getInstance().getPrefix());
                this.startPrepare();
            }
        }

        this.loadMap();
    }

    public QuickShotPlayer isWinner(){
        List<QuickShotPlayer> livingPlayers = new ArrayList<>();
        this.players.values()
                .stream()
                .filter(QuickShotPlayer::isAlive)
                .forEach(livingPlayers::add);

        if(livingPlayers.size() == 1){
            return livingPlayers.stream().findFirst().get();
        }

        return null;
    }

    public void checkForWinnnerAndInit(){
        QuickShotPlayer quickShotPlayer = this.isWinner();
        if(quickShotPlayer != null){

            Title winnerTitle = Title.builder()
                    .title("§a§l" + quickShotPlayer.getPlayer().getName())
                    .subtitle("§7hat das Spiel gewonnen!")
                    .fadeIn(10)
                    .stay(20)
                    .fadeOut(10)
                    .build();


            Location spawnLocation = GameFrame.getInstance().getGameFrameConfiguration().getSpawnLocation().toLocation();
            Bukkit.getOnlinePlayers().forEach(players -> {
                BukkitUtil.resetPlayer(players, true, true);
                BukkitUtil.clearInventory(players);
                players.setGameMode(GameMode.ADVENTURE);
                QuickShotScoreboard.getInstance().setLobbyScoreboard(players);
            });
            Bukkit.getScheduler().runTaskLater(QuickShot.getInstance(), () -> {
                Bukkit.getOnlinePlayers().forEach(BukkitUtil::showAllHiddenPlayers);
            }, 5);
            GameFrame.getInstance().getExecutorService().execute(() -> Bukkit.getOnlinePlayers().forEach(player -> {
                player.sendTitle(winnerTitle);
                player.teleport(spawnLocation);
                player.playSound(player.getLocation(), Sound.LEVEL_UP, 3, 1);
            }));


            this.setQuickShotState(QuickShotGame.QuickShotState.NONE);
            this.setCurrentStatus(GameStatus.ENDING);

            Countdown countdown = new EndCountdown();
            countdown.start();
            this.setCurrentCountdown(countdown);

        }
    }

    public void setSpectator(final Player player){
        BukkitUtil.clearInventory(player);
        BukkitUtil.resetPlayer(player, true, true);
        player.setAllowFlight(true);
        player.setFlying(true);
        player.teleport(this.players.values().iterator().next().getPlayer().getLocation()); // Random Player
        player.getInventory().setItem(0, SpectatorTeleporterItem.getInstance().build());

        QuickShotScoreboard.getInstance().setSpectator(player);
        BukkitUtil.hidePlayers(player, this::isPlayer);
        this.updateSpectatorInventory();
    }

    public void updateSpectatorInventory(){
        List<Player> list = new ArrayList<>();
        this.players.forEach((uuid, quickShotPlayer) -> list.add(quickShotPlayer.getPlayer()));
        SpectatorInventory.getInstance().updateInventory(list);
    }

    public Map<UUID, QuickShotPlayer> getPlayers() {
        return players;
    }

    public void registerPlayer(Player player){
        QuickShotPlayer quickShotPlayer = new QuickShotPlayer(player, 0, 3);
        this.players.put(player.getUniqueId(), quickShotPlayer);
    }

    public void dropPlayer(UUID uuid){
        this.players.remove(uuid);
        this.updateSpectatorInventory();

        if(this.getCurrentStatus() == GameStatus.RUNNING){
            checkForWinnnerAndInit();
        }
    }

    public boolean isPlayer(final Player player){
        return this.players.containsKey(player.getUniqueId()) && this.players.get(player.getUniqueId()).isAlive();
    }

    public boolean isFull(){
        return this.getGameSize().getAllowedPlayers() <= this.players.size();
    }

    public boolean isPlayer(UUID uuid){
        return this.players.containsKey(uuid) && this.players.get(uuid).isAlive();
    }

    public List<Location> getSpawnLocations() {
        return spawnLocations;
    }

    public void addSpawnLocation(Location location){
        this.spawnLocations.add(location);
    }

    public void clearSpawnLocation(){
        this.spawnLocations.clear();
    }

    public boolean isReady(){
        //return true;
        return this.players.size() >= 2;
    }

    public void startGameTimer(){
        this.timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Bukkit.getPluginManager().callEvent(new GameTickEvent(currentSeconds));
                currentSeconds++;
            }
        }, 1000, 1000);
    }

    public void stopGameTimer(){
        if(this.timer != null){
            this.timer.cancel();
            this.timer.purge();
        }

        this.timer = new Timer();
    }

    public void setStartedAt(long startedAt) {
        this.deathmatchTime = startedAt + TimeUnit.MINUTES.toMillis(5);
    }

    public void addDeathmatchLocation(Location location){
        this.deathmatchLocations.add(location);
    }

    public void setDeathmatchTime(long deathmatchTime) {
        this.deathmatchTime = deathmatchTime;
    }

    public void setSpawnProtection(final Player player){
        this.spawnProtection.add(player.getEntityId());
        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 500, 1, false, false));
    }

    public void removeSpawnProtection(final Player player){
        this.spawnProtection.remove((Object) player.getEntityId());
        player.removePotionEffect(PotionEffectType.INVISIBILITY);
    }

    public boolean hasSpawnProtection(final Player player){
        return this.spawnProtection.contains(player.getEntityId());
    }

    public Location getRandomSpawnLocation(){
        return this.spawnLocations.get(new Random().nextInt(this.spawnLocations.size()));
    }

    public Location getRandomDeathmatchLocation(){
        return this.deathmatchLocations.get(new Random().nextInt(this.deathmatchLocations.size()));
    }

    public enum QuickShotState {
        NONE,
        PREPARE,
        COLLECT,
        SHOP,
        FIGHT
    }
}
